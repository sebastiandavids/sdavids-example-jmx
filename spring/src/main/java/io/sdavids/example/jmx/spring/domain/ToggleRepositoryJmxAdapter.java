/*
 * Copyright (c) 2019, Sebastian Davids
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.sdavids.example.jmx.spring.domain;

import static io.sdavids.example.jmx.spring.dto.Toggle.toggleEnabled;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.StreamSupport.stream;

import io.sdavids.example.jmx.spring.dto.Toggle;
import io.sdavids.example.jmx.util.JmxUtils;
import java.util.Map;
import java.util.NoSuchElementException;
import javax.management.openmbean.CompositeData;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@ManagedResource(objectName = "io.sdavids.example.jmx.spring:type=Repository,name=Toggle") // <1>
@Component
public class ToggleRepositoryJmxAdapter { // <2>

  private final ToggleRepository toggleRepository;

  ToggleRepositoryJmxAdapter(ToggleRepository toggleRepository) {
    this.toggleRepository = requireNonNull(toggleRepository, "toggleRepository");
  }

  @ManagedAttribute // <3>
  public long getCount() {
    return toggleRepository.count();
  }

  @Nullable
  @ManagedAttribute // <4>
  public CompositeData getToggles() { // <5>
    Map<String, String> all =
        stream(toggleRepository.findAll().spliterator(), false)
            .collect(toMap(Toggle::getName, t -> Boolean.toString(t.isEnabled())));
    return JmxUtils.mapToCompositeData(all, "toggles", "all toggles");
  }

  @ManagedOperation // <6>
  public boolean toggle(String name) {
    requireNonNull(name, "name");
    Toggle found =
        toggleRepository.findByName(name).orElseThrow(() -> new NoSuchElementException(name));
    Toggle toggled = toggleEnabled(found);
    Toggle saved = toggleRepository.save(toggled);
    return saved.isEnabled();
  }
}
