/*
 * Copyright (c) 2019, Sebastian Davids
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.sdavids.example.jmx.spring.dto;

import static java.util.Objects.requireNonNull;

import javax.validation.ConstraintViolation;

public final class Violation {

  public static Violation violationFromConstraintViolation(ConstraintViolation<?> violation) {
    requireNonNull(violation, "violation");
    return new Violation(violation.getPropertyPath().toString(), violation.getMessage());
  }

  private final String fieldName;

  private final String message;

  private Violation(String fieldName, String message) {
    this.fieldName = requireNonNull(fieldName, "fieldName");
    this.message = requireNonNull(message, "message");
  }

  public String getFieldName() {
    return fieldName;
  }

  public String getMessage() {
    return message;
  }
}
