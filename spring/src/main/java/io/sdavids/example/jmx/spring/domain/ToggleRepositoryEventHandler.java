/*
 * Copyright (c) 2019, Sebastian Davids
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.sdavids.example.jmx.spring.domain;

import static java.util.Objects.requireNonNull;

import io.sdavids.example.jmx.spring.dto.Toggle;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

@RepositoryEventHandler
@Component
class ToggleRepositoryEventHandler {

  private final Validator validator;

  ToggleRepositoryEventHandler(Validator validator) {
    this.validator = requireNonNull(validator, "validator");
  }

  @HandleBeforeCreate
  void handleBeforeCreate(Toggle toggle) {
    validate(toggle);
    toggle.setId(toggle.getName());
  }

  @HandleBeforeSave
  void handleBeforeSave(Toggle toggle) {
    validate(toggle);
  }

  private void validate(Toggle toggle) {
    Set<ConstraintViolation<Toggle>> violations = validator.validate(toggle);
    if (violations.isEmpty()) {
      return;
    }
    throw new ConstraintViolationException(violations);
  }
}
