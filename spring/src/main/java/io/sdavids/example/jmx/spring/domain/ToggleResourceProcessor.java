/*
 * Copyright (c) 2019, Sebastian Davids
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.sdavids.example.jmx.spring.domain;

import static java.util.Objects.requireNonNull;

import io.sdavids.example.jmx.spring.dto.Toggle;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

@Component
class ToggleResourceProcessor implements ResourceProcessor<Resource<Toggle>> {

  private final RepositoryEntityLinks entityLinks;

  ToggleResourceProcessor(RepositoryEntityLinks entityLinks) {
    this.entityLinks = requireNonNull(entityLinks, "entityLinks");
  }

  @Override
  public Resource<Toggle> process(Resource<Toggle> resource) {
    resource.add(entityLinks.linkToCollectionResource(Toggle.class));
    return resource;
  }
}
