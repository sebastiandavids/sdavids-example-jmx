/*
 * Copyright (c) 2019, Sebastian Davids
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.sdavids.example.jmx.spring.dto;

import java.util.Objects;
import java.util.StringJoiner;
import javax.annotation.CheckReturnValue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import org.springframework.data.annotation.Id;
import org.springframework.lang.Nullable;

public final class Toggle {

  @CheckReturnValue
  public static Toggle toggleEnabled(Toggle toggle) {
    Toggle toggled = new Toggle();
    toggled.setId(toggle.getId());
    toggled.setName(toggle.getName());
    toggled.setDescription(toggle.getDescription());
    toggled.setEnabled(!toggle.isEnabled());
    return toggled;
  }

  @Id private String id;

  @NotBlank
  @Pattern(regexp = "^[A-Z_0-9]+$")
  private String name;

  private boolean enabled;

  private String description;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  @Nullable
  public String getDescription() {
    return description;
  }

  public void setDescription(@Nullable String description) {
    this.description = description;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Toggle toggle = (Toggle) o;
    return enabled == toggle.enabled
        && id.equals(toggle.id)
        && name.equals(toggle.name)
        && Objects.equals(description, toggle.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, enabled, description);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Toggle.class.getSimpleName() + "[", "]")
        .add("id='" + id + "'")
        .add("name='" + name + "'")
        .add("enabled=" + enabled)
        .add("description='" + description + "'")
        .toString();
  }
}
