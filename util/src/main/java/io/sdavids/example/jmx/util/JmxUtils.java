/*
 * Copyright (c) 2019, Sebastian Davids
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.sdavids.example.jmx.util;

import static java.util.Arrays.fill;
import static java.util.Objects.requireNonNull;
import static javax.management.openmbean.SimpleType.STRING;

import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;

public final class JmxUtils {

  @Nullable
  public static CompositeData mapToCompositeData(
      Map<String, String> map, String name, String description) {

    requireNonNull(map, "map");
    requireNonNull(map, "name");
    requireNonNull(map, "description");
    try {
      if (map.isEmpty()) {
        return null;
      }
      Set<String> keySet = map.keySet();
      int nKeys = keySet.size();
      String[] keys = keySet.toArray(new String[nKeys]);
      OpenType[] itemTypes = new OpenType[nKeys];
      fill(itemTypes, STRING);
      CompositeType propsType = new CompositeType(name, description, keys, keys, itemTypes);
      return new CompositeDataSupport(propsType, map);
    } catch (OpenDataException e) {
      return null;
    }
  }

  private JmxUtils() {}
}
