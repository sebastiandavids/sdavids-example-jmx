/*
 * Copyright (c) 2019, Sebastian Davids
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.sdavids.example.jmx.plain.domain;

import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofLocalizedDateTime;
import static java.time.format.FormatStyle.MEDIUM;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.Collections.emptyMap;
import static java.util.Collections.unmodifiableMap;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

public final class NameService {

  private final ReentrantLock nameLock;

  private String name;

  private Map<String, String> nameHistory;

  public NameService() {
    nameLock = new ReentrantLock();
    name = "";
    nameHistory = emptyMap();
  }

  String getName() {
    nameLock.lock();
    try {
      return name;
    } finally {
      nameLock.unlock();
    }
  }

  void setName(String name) {
    nameLock.lock();
    try {
      this.name = name;
      Map<String, String> history = new HashMap<>(nameHistory);
      history.put(now().format(ofLocalizedDateTime(MEDIUM)), name);
      nameHistory = unmodifiableMap(history);
    } finally {
      nameLock.unlock();
    }
  }

  Map<String, String> getNameHistory() {
    return nameHistory;
  }
}
