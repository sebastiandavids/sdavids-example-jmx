/*
 * Copyright (c) 2019, Sebastian Davids
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.sdavids.example.jmx.plain;

import static java.lang.management.ManagementFactory.getPlatformMBeanServer;
import static org.apache.logging.log4j.LogManager.getLogger;

import io.sdavids.example.jmx.plain.domain.NameService;
import io.sdavids.example.jmx.plain.domain.NameServiceJmxAdapter;
import java.lang.invoke.MethodHandles;
import java.util.concurrent.TimeUnit;
import javax.management.JMException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import org.apache.logging.log4j.Logger;

public final class JmxMain {

  private static final Logger LOGGER = getLogger(MethodHandles.lookup().lookupClass());

  public static void main(String[] args) throws JMException, InterruptedException {
    NameService service = new NameService();
    NameServiceJmxAdapter adapter = new NameServiceJmxAdapter(service);

    String objectName = "io.sdavids.example.jmx.plain:name=NameService"; // <1>
    MBeanServer server = getPlatformMBeanServer(); // <2>
    server.registerMBean(adapter, new ObjectName(objectName)); // <3>

    String name;
    do {
      name = adapter.getName();
      LOGGER.info("Current name: '{}'", name);
      TimeUnit.SECONDS.sleep(5);
    } while (!"exit".equals(name));
  }
}
