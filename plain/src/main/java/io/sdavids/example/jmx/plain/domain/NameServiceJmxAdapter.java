/*
 * Copyright (c) 2019, Sebastian Davids
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.sdavids.example.jmx.plain.domain;

import static io.sdavids.example.jmx.util.JmxUtils.mapToCompositeData;
import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;
import static java.util.Objects.requireNonNull;
import static org.apache.logging.log4j.LogManager.getLogger;

import java.lang.invoke.MethodHandles;
import java.util.List;
import javax.management.openmbean.CompositeData;
import org.apache.logging.log4j.Logger;

public final class NameServiceJmxAdapter implements NameServiceJmxAdapterMBean { // <1> <2>

  private static final Logger LOGGER = getLogger(MethodHandles.lookup().lookupClass());

  private static final List<String> exampleNames =
      unmodifiableList(asList("Bart", "Homer", "Lisa", "Maggie", "Marge"));

  private final NameService nameService;

  public NameServiceJmxAdapter(NameService nameService) {
    this.nameService = requireNonNull(nameService, "nameService");
  }

  @Override
  public List<String> getExampleNames() {
    return exampleNames;
  }

  @Override
  public String getName() {
    return nameService.getName();
  }

  @Override
  public CompositeData getNameHistory() {
    return mapToCompositeData(nameService.getNameHistory(), "nameHistory", "history of the names");
  }

  @Override
  public void sayHello() {
    LOGGER.info("Hello '{}'", nameService::getName);
  }

  @Override
  public void setName(String name) {
    nameService.setName(name);
  }
}
