/*
 * Copyright (c) 2019, Sebastian Davids
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.sdavids.example.jmx.plain.domain;

import java.io.IOException;
import java.util.List;
import javax.annotation.Nullable;
import javax.management.openmbean.CompositeData;

public interface NameServiceJmxAdapterMBean { // <1> <2>

  void sayHello() throws IOException; // <3> <4>

  String getName() throws IOException; // <5>

  void setName(String name) throws IOException; // <5>

  @Nullable
  CompositeData getNameHistory() throws IOException; // <6>

  List<String> getExampleNames() throws IOException; // <7> <8>
}
